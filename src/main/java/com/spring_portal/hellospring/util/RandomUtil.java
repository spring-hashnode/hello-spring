package com.spring_portal.hellospring.util;

import java.util.Random;

import org.springframework.data.mongodb.repository.MongoRepository;

public class RandomUtil {
    
    public static long generateLongId(MongoRepository<?, Long> mongoRepository) {
        long id;

        do {
            id = Math.abs(new Random().nextLong());
        }
        while (id != 0 && mongoRepository.existsById(id));

        return id;
    }
}
