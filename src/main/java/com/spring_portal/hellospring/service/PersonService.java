package com.spring_portal.hellospring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring_portal.hellospring.entity.Person;
import com.spring_portal.hellospring.repository.PersonRepository;
import com.spring_portal.hellospring.util.RandomUtil;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;
    
    public Person addPerson(Person person) {
        if (person.getId() == null) {
            person.setId(RandomUtil.generateLongId(personRepository));
        }

        return personRepository.save(person);
    }

    public Person getPerson(long id) {
        return personRepository.findById(id).orElse(null);
    }
}
