package com.spring_portal.hellospring.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.spring_portal.hellospring.entity.Person;

@Repository
public interface PersonRepository extends MongoRepository<Person, Long> {
    
}
