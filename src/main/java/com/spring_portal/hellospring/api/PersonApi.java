package com.spring_portal.hellospring.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.spring_portal.hellospring.entity.Person;
import com.spring_portal.hellospring.service.PersonService;

@RestController
@RequestMapping("/person")
public class PersonApi {
    
    @Autowired
    PersonService personService;

    @PostMapping("/add")
    public ResponseEntity<Person> addPerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.addPerson(person));
    }

    @GetMapping("/get")
    public ResponseEntity<Person> getPerson(@RequestParam Long id) {
        Person requestPerson = personService.getPerson(id);

        if (requestPerson == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(requestPerson);
    }
}
