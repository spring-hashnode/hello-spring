package com.spring_portal.hellospring.api;

import java.util.Map;
import java.util.UUID;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.spring_portal.hellospring.entity.Person;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/request")
public class RequestResponseApi {
    
    @GetMapping("/get")
    public ResponseEntity<Object> get(@RequestParam String first,
                                      @RequestParam(required = false) String second,
                                      @RequestParam(required = false, defaultValue = "2") Integer third,
                                      @RequestParam(required = false, name = "fourth") String fourthParam) {

        log.info("First = {} | Second = {} | Third = {} | Fourth = {}", first, second, third, fourthParam);

        // empty response with 200 success code
        return ResponseEntity.ok().build();
    }

    @GetMapping("/getBody")
    public ResponseEntity<Person> getBody(@RequestBody Person person) {

        log.info("Person object recieved -> {}", person);

        // response with 200 success code and a body
        return ResponseEntity.ok(person);
    }

    @PostMapping("/getPersonBody")
    public ResponseEntity<?> getOptionalBody(@RequestBody(required = false) Person person) {
        if (person == null) {
            return ResponseEntity.ok(Map.of(
                "defaultResponsePerson",
                Person.builder()
                .firstName("Response")
                .lastName("Person")
                .build()
            ));
        }
        
        // response with code 418
        return ResponseEntity.status(HttpStatus.I_AM_A_TEAPOT).body(person);
    }

    @ResponseBody
    @GetMapping("/getPersonResponse")
    public Person getPersonResponse() {
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.add("Request-UUID", UUID.randomUUID().toString());

        return Person.builder()
                .firstName("Response")
                .lastName("Person")
                .build();
    }
}
