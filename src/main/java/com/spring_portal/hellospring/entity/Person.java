package com.spring_portal.hellospring.entity;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@Document // 
@NoArgsConstructor
@AllArgsConstructor
public class Person {
    
    @Id
    Long id;

    String firstName;

    String lastName;

    Date dob;
}
